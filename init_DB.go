package codelab_DB

import (
	"database/sql"
	"github.com/go-gorp/gorp"
	_ "github.com/lib/pq"
	"fmt"
)

func InitDb() (*gorp.DbMap, error){
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
    "password=%s dbname=%s sslmode=disable",
    host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}

	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}

	dbmap.AddTableWithName(Candidate{}, "candidate").SetKeys(true, "ID")
	dbmap.AddTableWithName(Interviewer{}, "interviewer").SetKeys(true, "ID")
	dbmap.AddTableWithName(Interview{}, "interview").SetKeys(true, "ID")
	dbmap.AddTableWithName(Question{}, "question").SetKeys(true, "ID")
	dbmap.AddTableWithName(ChosenQuestion{}, "chosenquestion").SetKeys(false, "InterviewId", "QuestionId")

	return dbmap, err

}
