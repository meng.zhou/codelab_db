package codelab_DB

// ------------------------------ Create -----------------------------------

func AddQuestion(new_question Question) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	err = dbmap.Insert(&new_question)
	return err
}

func AddInterview(new_interview Interview) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	err = dbmap.Insert(&new_interview)
	return err
}

func AddCandidate(new_candidate Candidate) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	err = dbmap.Insert(&new_candidate)
	return err
}

func AddInterviewer(new_interviewer Interviewer) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	err = dbmap.Insert(&new_interviewer)
	return err
}


// ------------------------------ Get All ----------------------------------

func GetAllQuestion() ([]Question, error) {
	dbmap, err := InitDb()
	if err != nil {
		return nil, err
	}
	defer dbmap.Db.Close()

	var questions []Question
	_, err = dbmap.Select(&questions, "select * from question order by id")
	return questions, err
}

func GetAllInterview() ([]Interview, error) {
	dbmap, err := InitDb()
	if err != nil {
		return nil, err
	}
	defer dbmap.Db.Close()

	var interviews []Interview
	_, err = dbmap.Select(&interviews, "select * from interview order by id")
	return interviews, err
}

func GetAllCandidate() ([]Candidate, error) {
	dbmap, err := InitDb()
	if err != nil {
		return nil, err
	}
	defer dbmap.Db.Close()

	var candidates []Candidate
	_, err = dbmap.Select(&candidates, "select * from candidate order by id")
	return candidates, err
}

func GetAllInterviewer() ([]Interviewer, error) {
	dbmap, err := InitDb()
	if err != nil {
		return nil, err
	}
	defer dbmap.Db.Close()

	var interviewers []Interviewer
	_, err = dbmap.Select(&interviewers, "select * from interviewer order by id")
	return interviewers, err
}


// ------------------------- Update -------------------------------------------
func EditQuestion(question Question) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	_, err = dbmap.Update(&question)
	return err
}

func EditInterview(interview Interview) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	_, err = dbmap.Update(&interview)
	return err
}

func EditCandidate(candidate Candidate) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	_, err = dbmap.Update(&candidate)
	return err
}

func EditInterviewer(interviewer Interviewer) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	_, err = dbmap.Update(&interviewer)
	return err
}

// --------------------------- Delete ---------------------------------------
func DeleteQuestion(id int64) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()
	_, err = dbmap.Exec("update question set active = false where id=$1", id)
	return err
}



// ----------------------------------Editor -----------------------------------

func AddQuestionToInterview(interview_id int64, question_id int64) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	_, err = dbmap.Exec("insert into chosenquestion(interview_id, question_id) values ($1, $2)", interview_id, question_id)
	return err
}

func DeleteQuestionFromInterview(interview_id int64, question_id int64) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()
	_, err = dbmap.Exec("delete from chosenquestion where interview_id=$1 and question_id=$2", interview_id, question_id)
	return err
}

func StoreQuestionSolution(chosenquestion ChosenQuestion) error {
	dbmap, err := InitDb()
	if err != nil {
		return err
	}
	defer dbmap.Db.Close()

	_, err = dbmap.Update(&chosenquestion)
	return err
}

func GetQuestionSolution(interview_id int64, question_id int64) (string, error){
	dbmap, err := InitDb()
	if err != nil {
		return "", err
	}
	defer dbmap.Db.Close()

	solution, err := dbmap.SelectStr("select solution from chosenquestion where interview_id=$1 and question_id=$2", interview_id, question_id)
	return solution, err
}

func GetQuestionInInterview(interview_id int64) ([]Question, error){
	dbmap, err := InitDb()
	if err != nil {
		return nil, err
	}
	defer dbmap.Db.Close()

	var questions []Question
	_, err = dbmap.Select(&questions, "select q.* from chosenquestion as c inner join question as q on c.question_id=q.id where c.interview_id=$1", interview_id)

	return questions, err
}

func GetQuestionNotInInterview(interview_id int64) ([]Question, error){
	dbmap, err := InitDb()
	if err != nil {
		return nil, err
	}
	defer dbmap.Db.Close()

	var questions []Question
	_, err = dbmap.Select(&questions, "select * from question where id not in (select question_id from chosenquestion where interview_id=$1)", interview_id)

	return questions, err
}




