CREATE TABLE Candidate (
	id bigserial PRIMARY KEY,
	name TEXT NOT NULL,
	active BOOLEAN NOT NULL DEFAULT TRUE,
	token TEXT DEFAULT '',
	email TEXT DEFAULT '',
	phone TEXT DEFAULT ''
);

CREATE TABLE Interviewer (
	id bigserial PRIMARY KEY,
	name TEXT NOT NULL,
	active BOOLEAN NOT NULL DEFAULT TRUE,
	token TEXT DEFAULT '',
	email TEXT DEFAULT '',
	phone TEXT DEFAULT ''
);

CREATE TABLE Interview (
	id bigserial PRIMARY KEY,
	candidate_id bigint REFERENCES Candidate(id),
	interviewer_id bigint REFERENCES Interviewer(id),
	start_time bigint DEFAULT 0,
	active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE Question (
	id bigserial PRIMARY KEY,
	title TEXT NOT NULL,
	description TEXT NOT NULL,
	example TEXT DEFAULT '',
	topic TEXT DEFAULT '',
	solution TEXT DEFAULT '',
	level bigint NOT NULL,
	active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE ChosenQuestion (
	interview_id bigint REFERENCES Interview(id),
	question_id bigint REFERENCES Question(id),
	solution TEXT DEFAULT '',
	PRIMARY KEY(interview_id, question_id)
);