package codelab_DB

import (
	_ "github.com/lib/pq"
)

type Candidate struct{
	ID int64 `db:"id"`
	Name string `db:"name,notnull"`
	Active bool `db:"active,notnull,default:true"`
	Token string `db:"token"`
	Email string `db:"email"`
	Phone string `db:"phone"`
}

type Interviewer struct{
	ID int64 `db:"id"`
	Name string `db:"name,notnull"`
	Active bool `db:"active,notnull,default:true"`
	Token string `db:"token"`
	Email string `db:"email"`
	Phone string `db:"phone"`
}

type Interview struct{
	ID int64 `db:"id"`
	CandidateId int64 `db:"candidate_id,notnull"`
	InterviewerId int64 `db:"interviewer_id,notnull"`
	StartTime int64 `db:"start_time"`
	Active bool `db:"active,notnull,default:true"`
}

type Question struct{
	ID int64 `db:"id"`
	Title string `db:"title,notnull"`
	Description string `db:"description,notnull"`
	Example string `db:"example,default:''"`
	Topic string `db:"topic,default:''"`
	Solution string `db:"solution,default:''"`
	Level int64 `db:"level,notnull"`
	Active bool `db:"active,notnull,default:true"`
}

type ChosenQuestion struct{
	InterviewId int64 `db:"interview_id,notnull"`
	QuestionId int64 `db:"question_id,notnull"`
	Solution string `db:"solution,default:''"`
}
